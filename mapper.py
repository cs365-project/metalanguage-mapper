#/bin/python
import sys
data = [line[:-1].split() for line in open('giza-script/dictionary.t3.final')]
dictionary = {}
for i in data:
	dictionary[(i[0], i[1])] = float(i[2])
temp_vcb = [line[:-1].split() for line in open('giza-script/corp.tok.low.en.vcb')]
source_vcb = {}
for i in temp_vcb:
	source_vcb[i[1]] = i[0]
temp_vcb = [line[:-1].split() for line in open('giza-script/corp.tok.low.meta.vcb')]
target_vcb = {}
for i in temp_vcb:
	target_vcb[i[0]] = i[1]
f = open(sys.argv[1]).readlines()
output = ''
for line in f:
	for i in line.split():
		max = 0
		res = ''
		for j in set(dictionary):
			if source_vcb.get(i) == j[0]:
				if dictionary[(j[0], j[1])] >= max:
					max = dictionary[(j[0], j[1])]
					res = j[1]
		if target_vcb.get(res) is not None:
			output = output + " " + target_vcb.get(res)
	output = output[1:]+'\n'
# print output[:-1]
open('parsed', 'w').write(output[:-1])