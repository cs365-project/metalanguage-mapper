filename="${1##*/}"
sed -r 's/([ ,\.])([A-Za-z]|[0-9]+) *\* *([A-Za-z]|[0-9]+)([ ,\.])/\1varSize\4/g' $1 > './formatted_data/'$filename'.frm'  
sed -i -r 's/([ ,\.])([A-Za-z]|[0-9]+) *[xX] *([A-Za-z]|[0-9]+)([ ,\.])/\1varSize\4/g' './formatted_data/'$filename'.frm' 
sed -i -r 's/([ ,\.])[A-Z]([ ,\.])/\1varSymbol\2/g' './formatted_data/'$filename'.frm'
sed -i -r 's/([ ,\.])[0-9]+([ ,\.])/\1varNum\2/g' './formatted_data/'$filename'.frm' 