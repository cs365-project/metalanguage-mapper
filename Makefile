.PHONY: build
.DEFAULT_GOAL: build
build:
	mkdir -p formatted_data/
	git submodule init
	git submodule update
	make -C giza-script/
	cp corpus-data/raw* giza-script/
clean:
	make clean -C giza-script/
	rm -f *.dat parsed parserOutput temp test.out parseTree.dot 
