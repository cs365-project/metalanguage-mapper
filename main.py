#!/bin/python 
import sys
import os
if len(sys.argv) == 1:
	f = sys.stdin
	open("formatted_data/stdin", "w").write(f.read())
	os.system("./prepare_data.sh formatted_data/stdin")
	filename = 'formatted_data/stdin'
else:
	os.system("./prepare_data.sh "+sys.argv[1])
	filename = sys.argv[1]
# orig = open(filename).readlines()
os.system("./formatter.sh "+filename)
# after = open(filename+".frm").readlines()
os.system("giza-script/europarl/tools/tokenizer.perl -l en < "+ filename+".frm"+ "> "+filename+".frm.tok")
os.system("tr '[:upper:]' '[:lower:]' < "+filename+".frm.tok" +" > " + filename+".frm.tok.meta")
os.system("python mapper.py "+filename+".frm.tok.meta")
print open('parsed').read()
os.system("../metalanguage-parser/parser parsed > parserOutput; cat parserOutput")