# Metalanguage Mapper
--------------------
Currently it is formatting data only.The formatted data goes to `formatted_data/` folder with extension `.fil`.

To run the mapper(currently formatter):
```bash
$ chmod +x prepare_data.sh
$ ./prepare_data.sh filename
```
The file should be in English langauge. The program will load data from `valid_words.dat` and will generate sparse sentences using the data into a `.fil` file in folder `formatted_data/`.