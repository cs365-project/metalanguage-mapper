#!/usr/bin/python
import sys

validWords = ['varNum', 'varSize', 'varSymbol']
f = open('corpus-data/valid_words.dat','r')
data = f.read().splitlines()
data = [line[:-1] for line in data]
for lines in data:
	validWords += lines.split()
if len(sys.argv) != 2:
	f = sys.stdin
	filename = "formatted_data/stdin"
else:
	filename = sys.argv[1]
	f = open(filename, 'r')
savename = filename+'.fil'
data = f.read().splitlines()
data = [line[:-1] for line in data]
lines = []
for line in data:
	temp = line.split()
	genList = []
	for word in temp:
		if any(word == s for s in validWords):
			genList.append(word)
	lines.append(' '.join(genList)+'.')
# print '\n'.join(lines)
open(savename, 'w').write('\n'.join(lines))
