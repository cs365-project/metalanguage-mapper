#/bin/bash
if [ $# -ne 1 ]
then
	echo "Filename is required!.";
	exit 0;
fi
filename="${1##*/}"
mkdir -p formatted_data
chmod +x formatter.sh
./formatter.sh $1
python filter.py './formatted_data/'$filename'.frm'
